# AlgorithmPractice

### 介绍
这个仓库用来存放一些我做过的练习，主要用Java语言编写，练习涵盖：

1. [《算法笔记》练习题](https://sunnywhy.com/sfbj)
2. 《算法竞赛》练习题
3. 各个OJ平台的题目
* [POJ](http://poj.org/problemlist)
* [HDUOJ](https://acm.hdu.edu.cn/)
* [LeetCode (力扣)](https://leetcode.cn/problemset/)
* [洛谷](https://www.luogu.com.cn/problem/list)

### 部分题解会更新在我的CSDN博客上
[个人博客地址](https://blog.csdn.net/weixin_43527493)