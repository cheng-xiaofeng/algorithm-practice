package team.cbx.SFBJpractice.practice3;

import java.util.Scanner;

public class Practice306 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        int num1 = num / 100;
        int num2 = (num - num1 * 100) / 10;
        int num3 = num % 10;
        if (num1*num1*num1 + num2*num2*num2 + num3*num3*num3 == num) {
            System.out.println("YES");
        } else {
            System.out.println("NO");
        }
    }
}