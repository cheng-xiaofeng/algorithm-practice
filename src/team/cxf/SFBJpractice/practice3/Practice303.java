package team.cbx.SFBJpractice.practice3;

import java.util.Scanner;

public class Practice303 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String num = sc.nextLine();
        int[] arr = new int[Integer.parseInt(num)];
        String str = sc.nextLine();
        String[] strArr = str.split(" ");
        for (int i = 0; i < strArr.length; i++) {
            arr[i] = Integer.parseInt(strArr[i]);
        }
        Boolean flag = true;
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] < arr[i-1]) {
                flag = false;
            }
        }
        System.out.println(flag ? "YES" : "NO");
    }
}
