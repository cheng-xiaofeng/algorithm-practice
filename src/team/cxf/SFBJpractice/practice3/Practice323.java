package team.cbx.SFBJpractice.practice3;

import java.util.Scanner;

public class Practice323 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        for (int i = 0; i < n; i++) {
            int numSpace = (i < n / 2) ? (n - i) : (i + 1);
            for (int j = 0; j < numSpace; j++) {
                if (i == j || j == n - 1 - i) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
