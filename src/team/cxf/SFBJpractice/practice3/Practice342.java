package team.cbx.SFBJpractice.practice3;

import java.util.Scanner;

public class Practice342 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        char[] chars = str.toCharArray();
        int res = 0;
        for (int i = 0, j = chars.length-1; i < chars.length; i++, j--) {
            int num = Integer.parseInt(String.valueOf(chars[j]));
            res += (Math.pow(2, i) * num);
        }
        System.out.println(res);
    }
}
