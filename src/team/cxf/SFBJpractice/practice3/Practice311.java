package team.cbx.SFBJpractice.practice3;

import java.util.Scanner;

public class Practice311 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = Integer.parseInt(sc.nextLine());
        String str = sc.nextLine();
        int x = Integer.parseInt(sc.nextLine());
        String[] strArr = str.split(" ");
        for (int i = 0; i < num; i++) {
            if(strArr[i].equals(String.valueOf(x))) {
                System.out.println(i+1);
                return;
            }
        }
        System.out.println("NO");
    }
}
