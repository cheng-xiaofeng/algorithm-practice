package team.cbx.SFBJpractice.practice3;

import java.util.Scanner;

public class Practice322 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        // row control
        for (int i = 0; i < n; i++) {
            // first row
            if (i == 0) {
                System.out.println("*");
            } else if (i == n - 1) {
                // last row
                for (int j = 0; j < n; j++) {
                    System.out.print("*");
                }
                System.out.println();
            } else {
                // between first and last row
                for (int j = 0; j <= i; j++) {
                    if (j == 0 || j == i) {
                        System.out.print("*");
                    } else {
                        System.out.print(" ");
                    }
                }
                System.out.println();
            }
        }
    }
}
