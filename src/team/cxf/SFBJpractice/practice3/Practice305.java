package team.cbx.SFBJpractice.practice3;

import java.util.Scanner;

public class Practice305 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        int num1 = num / 100;
        int num2 = (num - num1 * 100) / 10;
        int num3 = num % 10;
        System.out.println(num1 + " " + num2 + " " + num3);
    }
}
