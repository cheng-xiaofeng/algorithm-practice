package team.cbx.SFBJpractice.practice3;

import java.util.Scanner;

public class Practice322_2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        // first row
        System.out.println("*");
        // row control
        for (int i = 0; i < n - 2; i++) {
            System.out.print("*");
            for (int j = 1; j <= i; j++) {
                System.out.print(" ");
            }
            System.out.println("*");
        }
        // last row
        for (int j = 0; j < n; j++) {
            System.out.print("*");
        }
        System.out.println();
    }
}
