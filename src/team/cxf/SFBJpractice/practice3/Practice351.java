package team.cbx.SFBJpractice.practice3;

import java.util.Scanner;

public class Practice351 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        char[] charArray = str.toCharArray();
        for (int i = 0, j = charArray.length-1; j - i > -1; i++, j--) {
            if (charArray[i] != charArray[j]) {
                System.out.println("NO");
                return;
            }
        }
        System.out.println("YES");
    }
}
