package team.cbx.SFBJpractice.practice3;

import java.util.Scanner;

public class Practice343 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        String[] strings = str.split(" ");
        int num = Integer.parseInt(strings[0]);
        int scaleK = Integer.parseInt(strings[1]);
        // 10进制 转 2进制
        StringBuilder tempStr = new StringBuilder();
        do {
            tempStr.append(num % scaleK).append(',');
            num /= scaleK;
        } while (num != 0);
        String scaleNumStr = tempStr.substring(0, tempStr.length()-1);
        String[] split = scaleNumStr.split(",");
        for (int i = split.length-1; i >= 0; i--) {
            int s = Integer.parseInt(split[i]);
            if (s <= 9) {
                System.out.print(s);
            } else {
                System.out.print((char) (s - 10 + ((int) 'A')));
            }
        }
    }
}
