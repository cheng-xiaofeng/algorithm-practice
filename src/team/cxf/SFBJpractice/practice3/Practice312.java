package team.cbx.SFBJpractice.practice3;

import java.util.Scanner;

public class Practice312 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = Integer.parseInt(sc.nextLine());
        String str = sc.nextLine();
        int x = Integer.parseInt(sc.nextLine());
        String[] strArr = str.split(" ");
        int count = 0;
        for (int i = 0; i < num; i++) {
            if(strArr[i].equals(String.valueOf(x))) {
                count++;
            }
        }
        System.out.println(count);
    }
}
