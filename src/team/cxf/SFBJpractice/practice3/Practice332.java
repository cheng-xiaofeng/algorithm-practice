package team.cbx.SFBJpractice.practice3;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Practice332 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        int days = Integer.parseInt(scanner.nextLine());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = sdf.parse(str);
            date.setTime(date.getTime() + ((long) days * 24 * 60 * 60 * 1000));
            System.out.print(sdf.format(date));
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
}
