package team.cbx.SFBJpractice.practice3;

import java.util.Scanner;

public class Practice307 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String num = sc.nextLine();
        String[] numArr = num.split(" ");
        int a = Integer.parseInt(numArr[0]);
        int b = Integer.parseInt(numArr[1]);
        String str = "";
        for (int i = a; i < b + 1; i++) {
            int num1 = i / 100;
            int num2 = (i - num1 * 100) / 10;
            int num3 = i % 10;
            if (num1*num1*num1 + num2*num2*num2 + num3*num3*num3 == i) {
                str = str + String.valueOf(i) + " ";
            }
        }
        System.out.println(str == "" ? "NO" : str.substring(0,str.length() - 1));
    }
}