package team.cbx.SFBJpractice.practice3;

import java.util.Scanner;

public class Practice313 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = Integer.parseInt(sc.nextLine());
        String str = sc.nextLine();
        int k = Integer.parseInt(sc.nextLine());
        String[] strArr = str.split(" ");
        int count = 0;
        for (int i = 0; i < num; i++) {
            for (int j = i + 1 ; j < num; j++) {
                if (Integer.parseInt(strArr[i]) + Integer.parseInt(strArr[j]) == k) {
                    count++;
                }
            }
        }
        System.out.println(count);
    }
}