package team.cbx.SFBJpractice.practice3;

import java.util.Scanner;

public class Practice308 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int res = 1;
        while(n != 0){
            res = (res * 2) % 1007;
            n--;
        }
        System.out.println(res);
    }
}
