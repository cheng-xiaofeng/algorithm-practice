package team.cbx.SFBJpractice.practice3;

import java.util.Scanner;

public class Practice301 {

    public static void main(String[] args) {
        int count = 0;
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        while (num != 1) {
            // 偶数
            if (num % 2 == 0) {
                num = num / 2;
            } else {// 奇数
                num = (num * 3 + 1) / 2;
            }
            count++;
        }
        System.out.println(count);
    }
}
