package team.cbx.SFBJpractice.practice3;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Practice335 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str1 = scanner.nextLine();
        String str2 = scanner.nextLine();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date1 = sdf.parse(str1);
            Date date2 = sdf.parse(str2);
            if (date1.before(date2)) {
                System.out.print("YES");
            } else {
                System.out.print("NO");
            }
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
}
