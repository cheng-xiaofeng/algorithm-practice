package team.cbx.SFBJpractice.practice3;

import java.util.Scanner;

public class Practice341 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        StringBuilder str = new StringBuilder();
        while(num != 0) {
            str.append(num % 2);
            num /= 2;
        }
        System.out.println(str.reverse());
    }
}
