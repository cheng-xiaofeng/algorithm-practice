package team.cbx.SFBJpractice.practice3;

import java.util.Scanner;

public class Practice331 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        if (n%400 == 0 || (n%100 != 0 && n%4 == 0)) {
            System.out.println("YES");
        } else {
            System.out.println("NO");
        }
    }
}
