package team.cbx.SFBJpractice.practice3;

import java.util.Scanner;

public class Practice304 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String num = sc.nextLine();
        int[] arr = new int[Integer.parseInt(num)];
        String str = sc.nextLine();
        String[] strArr = str.split(" ");
        for (int i = 0; i < strArr.length; i++) {
            arr[i] = Integer.parseInt(strArr[i]);
        }
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 != 0) {
                count += arr[i];
            }
        }
        System.out.println(count);
    }
}
