package team.cbx.SFBJpractice.practice3;

import java.util.Calendar;
import java.util.Scanner;

public class Practice334 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        String[] dateArr = str.split("-");
        Calendar calendar = Calendar.getInstance();
        calendar.set(Integer.parseInt(dateArr[0]), Integer.parseInt(dateArr[1]) - 1, Integer.parseInt(dateArr[2]));
        System.out.println(calendar.get(Calendar.DAY_OF_YEAR));
    }
}
