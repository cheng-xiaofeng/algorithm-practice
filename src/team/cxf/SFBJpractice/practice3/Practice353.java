package team.cbx.SFBJpractice.practice3;

import java.util.Scanner;

public class Practice353 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        String[] split = str.split(" ");
        for (int i = 0; i < split.length; i++) {
            char[] chars = split[i].toCharArray();
            for (int j = chars.length-1; j >= 0; j--) {
                System.out.print(chars[j]);
            }
            if (i != split.length-1) {
                System.out.print(" ");
            }
        }
    }
}
