package team.cbx.SFBJpractice.practice3;

import java.util.Scanner;

public class Practice321 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        // row control
        for (int i = 0; i < n; i++) {
            // col control
            for (int j = 0; j <= i; j++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }
}
