package team.cbx.SFBJpractice.practice3;

import java.util.Scanner;

public class Practice344 {
    public static void main(String[] args) {
        int res = 0;
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        String[] strings = str.split(" ");
        String numK = strings[0];
        int scale = Integer.parseInt(strings[1]);
        char[] chars = numK.toCharArray();
        for (int i = 0, j = chars.length-1; i < chars.length; i++, j--) {
            if (((int) chars[j]) >= ((int) '0') && ((int) chars[j]) <= ((int) '9')) {
                res += (int) (Math.pow(scale, i) * (Integer.parseInt(String.valueOf(chars[j]))));
            } else {
                res += (int) (Math.pow(scale, i) * (((int) chars[j]) - ((int) 'A') + 10));
            }
        }
        System.out.println(res);
    }
}
