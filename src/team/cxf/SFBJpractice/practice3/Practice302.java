package team.cbx.SFBJpractice.practice3;

import java.util.Scanner;

public class Practice302 {
    public static void main(String[] args) {
        int a = 0, b = 0, c = 0;
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        String[] strArr = str.split(" ");
        if (strArr.length != 0) {
            a = Integer.parseInt(strArr[0]);
            b = Integer.parseInt(strArr[1]);
            c = Integer.parseInt(strArr[2]);
        }
        if ((a + b > c) && (a + c > b) && (b + c > a)) {
        System.out.println("YES");            
        } else {
            System.out.println("NO");
        }
    }
}
