package team.cbx.SFJSractice.practice2;


import java.util.Scanner;

public class Priactice226 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        String[] splitArr1 = str.split(" ");
        int N = Integer.parseInt(splitArr1[0]);
        int M = Integer.parseInt(splitArr1[1]);
        int[] arr = new int[N];
        String str2 = sc.nextLine();
        String[] splitArr2 = str2.split(" ");
        for (int i = 0; i < N; i++) {
            arr[i] = Integer.parseInt(splitArr2[i]);
        }
        sc.close();

        int result = findMinimumLargestSubarraySum(arr, M);
        System.out.println(result);
    }

    /**
     * @Description <二分搜索>
     * @Date 16:58 2024/1/21
     * @Param [A 序列, M 最大划分序列数, maxSum 序列和最大值]
     * @return boolean
     **/
    private static boolean canSplit(int[] A, int M, int maxSum) {
        int currentSum = 0;
        int count = 1;
        for (int num : A) {
            if (currentSum + num > maxSum) {
                count++;
                currentSum = num;
                if (count > M) {
                    return false;
                }
            } else {
                currentSum += num;
            }
        }
        return true;
    }

    /**
     * @Description <求解方法>
     * @Date 17:00 2024/1/21
     * @Param [A 序列, M 最大划分序列数]
     * @return int
     **/
    private static int findMinimumLargestSubarraySum(int[] A, int M) {
        int start = 0;
        int end = 0;
        for (int num : A) {
            start = Math.max(start, num);
            end += num;
        }

        int result = end;
        while (start <= end) {
            int mid = start + (end - start)/2;
            if (canSplit(A, M, mid)) {
                result = mid;
                end = mid - 1;
            } else {
                start = mid + 1;
            }
        }

        return result;
    }
}
