package team.cbx.SFJSractice.practice2;


import java.util.Scanner;

import static java.util.Arrays.sort;

public class Practice225 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        String[] splitArr = str.split(" ");
        int[] arr = new int[splitArr.length];
        for (int i = 0; i < splitArr.length; i++) {
            if (splitArr[i] != null && !splitArr[i].equals("")) {
                arr[i] = Integer.parseInt(splitArr[i]);
            } else {
                arr[i] = 0;
            }
        }
        sort(arr);
        // 从头到尾处理arr[i]的每个元素,在arr[i]后面的数中二分查找是否存在一个等于m-arr[i]的数
        int m = Integer.parseInt(sc.nextLine());
        for (int i = 0; i < arr.length; i++) {
            int index = binarySearch(arr, i, arr.length - 1, m - arr[i]);
            if (index != -1) {
                System.out.println(arr[i] + " " + arr[index]);
                break;
            }
        }
    }

    /**
     * @Description <二分求解方法>
     * @Date 14:57 2024/1/21
     * @Param [arr, start, end, m]
     * @return int
     **/
    static int binarySearch(int[] arr, int start, int end, int m) {
        while (start <= end) {
            int mid = (start + end) / 2;
            if (arr[mid] == m) {
                return mid;
            } else if (arr[mid] < m) {
                start = mid + 1;
            } else {
                end = mid - 1;
            }
        }
        return -1;
    }
}
