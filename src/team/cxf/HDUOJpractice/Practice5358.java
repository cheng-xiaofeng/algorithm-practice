package team.cbx.HDUOJpractice;

import java.util.Scanner;

/**
 * @description: HDUOJ 5358
 * @date: 2024/9/17 10:37
 * @author: Bingxiang.Cheng
 * @email: bingxiang.cheng@jumipm.com
 * @version: 1.0
 */
public class Practice5358 {
    static long[] a = new long[100010];
    static long[] sum = new long[100010];

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int T = scanner.nextInt();
        while (T-- > 0) {
            int n = scanner.nextInt();
            for (int i = 1; i <= n; i++) {
                a[i] = scanner.nextLong();
                sum[i] = sum[i - 1] + a[i];
            }
            long ans = 0;
            for (long i = 1; i <= 35; i++) { // 都要用long
                long l = 1, r = 0;
                long minn = 1L << (i - 1), maxx = (1L << i) - 1;
                if (i == 1)
                    minn = 0;
                for (long x = 1; x <= n; x++) {
                    l = Math.max(l, x);
                    while (l <= n && sum[(int) l] - sum[(int) (x - 1)] < minn) l++;
                    r = Math.max(r, l - 1); // r=l-1是一个小技巧，可以判断他是否满足题目所给的条件
                    while (r + 1 <= n && sum[(int) (r + 1)] - sum[(int) (x - 1)] <= maxx
                            && sum[(int) (r + 1)] - sum[(int) (x - 1)] >= minn)
                        r++;

                    if (l > r)
                        continue;
                    ans = ans + (x * (r - l + 1) + (r + l) * (r - l + 1) / 2) * i;
                }
            }
            System.out.println(ans);
        }
        scanner.close();
    }
}

