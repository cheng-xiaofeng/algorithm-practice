package team.cbx.POJpriactice;

import java.math.BigInteger;
import java.util.Scanner;

/**
 * @description: POJ 1426 Example1
 * @date: 2024/9/8 14:56
 * @author: Bingxiang.Cheng
 * @email: bingxiang.cheng@jumipm.com
 * @version: 1.0
 */
public class Practice1426Simple1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        BigInteger bigN = BigInteger.valueOf(n);
        BigInteger number = BigInteger.valueOf(n+1); // Start with n in binary
        while (true) {
            if (number.mod(bigN).equals(BigInteger.ZERO)) {
                String numStr = number.toString();
                int count = numStr.length();
                for (char c : numStr.toCharArray()) {
                    if (c == '0' || c == '1') {
                        count--;
                    }
                    if (count < 1) {
                        System.out.println(number);
                        return;
                    }
                }
            }
            number = number.add(BigInteger.ONE);
        }
    }
}
