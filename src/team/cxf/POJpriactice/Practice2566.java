package team.cbx.POJpriactice;

import java.util.Scanner;

/**
 * @description: POJ 2566
 * @date: 2024/9/17 10:37
 * @author: Bingxiang.Cheng
 * @email: bingxiang.cheng@jumipm.com
 * @version: 1.0
 */
public class Practice2566 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int caseNum = Integer.parseInt(sc.nextLine());
        String[] strings = new String[caseNum * 2];
        for (int i = 0; i < caseNum * 2; i++) {
            strings[i] = sc.nextLine();
        }
        for (int i = 0; i < caseNum; i++) {
            String[] split1 = strings[i * 2].split(" ");
            int N = Integer.parseInt(split1[0]);
            int S = Integer.parseInt(split1[1]);
            String[] split2 = strings[i * 2 + 1].split(" ");
            int count = Integer.parseInt(split2[0]);
            int minLength = N + 1;
            for (int j = 0, k = 1; count > 0 && j < N; ) {
                if (count < S && k < N) {
                    count += Integer.parseInt(split2[k++]);
                } else if (count >= S) {
                    minLength = Math.min(minLength, k - j);
                    count -= Integer.parseInt(split2[j++]);
                } else {
                    j++;
                }
            }
            if (minLength == N + 1) {
                System.out.println(0);
            } else {
                System.out.println(minLength);
            }
        }
    }
}
