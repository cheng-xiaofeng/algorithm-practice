package team.cbx.POJpriactice;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

/**
 * @description: POJ 1426
 * @date: 2024/9/17 10:37
 * @author: Bingxiang.Cheng
 * @email: bingxiang.cheng@jumipm.com
 * @version: 1.0
 */
public class Practice1426 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        // BFS Queue: store the current binary number as a string and its remainder when divided by n
        Queue<String[]> queue = new LinkedList<>();

        // Start with "1" (since the highest bit cannot be 0)
        queue.add(new String[]{"1", "1"});

        // BFS search
        while (!queue.isEmpty()) {
            String[] current = queue.poll();// 获取并移除队列头部的元素
            String binary = current[0];
            String remainderStr = current[1];
            int remainder = Integer.parseInt(remainderStr);

            // Check if the current binary number is a multiple of n
            if (remainder == 0) {
                System.out.println(binary);
                return;
            }

            // Append "0" and "1" to the current binary number and compute new remainders
            queue.add(new String[]{binary + "0", String.valueOf((remainder * 10) % n)});
            queue.add(new String[]{binary + "1", String.valueOf((remainder * 10 + 1) % n)});
        }

        // In case no multiple is found (should not happen for the given problem constraints)
        System.out.println("No multiple found");
    }
}
