package team.cbx.POJpriactice;

import java.math.BigDecimal;
import java.util.*;

/**
 * @description: POJ 3278
 * @date: 2024/9/17 10:37
 * @author: Bingxiang.Cheng
 * @email: bingxiang.cheng@jumipm.com
 * @version: 1.0
 */
public class Practice3278 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        String[] strArr = str.split(" ");
        int start = Integer.parseInt(strArr[0]);
        int end = Integer.parseInt(strArr[1]);
        if (start > end) {
            System.out.print(start - end);
        } else {
            BigDecimal count = BigDecimal.ZERO;
            List<Integer> numlist = new ArrayList<Integer>();
            Map<Integer, Boolean> map = new HashMap<Integer, Boolean>();
            numlist.add(start);
            while (true) {
                Integer queueFirst = numlist.get(0);
                if (map.get(queueFirst) != null && map.get(queueFirst)) {
                    continue;
                }
                if (queueFirst == end) {
                    break;
                }
                numlist.remove(0);
                numlist.add(queueFirst+1);
                numlist.add(queueFirst-1);
                numlist.add(queueFirst*2);
                map.put(queueFirst, true);
                count = count.add(BigDecimal.ONE);
            }
            System.out.print((int) Math.floor(Math.log(count.intValue()) / Math.log(3)));
        }
    }
}
